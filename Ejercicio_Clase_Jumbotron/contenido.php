<?php

//Funciones de las vistas. Esto es lo que controla el aspecto en pantalla
              /*"cabecera" => "Ejemplo",
                "texto" => ["primero", "segundo", "tercero"]*/

function jumbotron($datos){
    echo '<div class="jumbotron"><div class="container">';
    echo "<h1>$datos[cabecera]</h1>";//Esto coloca el titulo
    
    foreach($datos["texto"] as $valores){ //Esto coloca los textos
        echo "<p>$valores</p>";
    }
    echo "</div></div>";
}
//Funcion para el control del formulario
function control($datos){ //Este arg es un array asociativo
    //Ponemos en variables lo que queremos que vaya cambiando
    $nombre=" ";
    $texto=" ";
    $id=" ";
    $placeholder="Escribe aqui el dato...";
    $tipo="text";
    extract($datos,EXTR_OVERWRITE);//Funcion extract utiliza las variable con valores
    //que coinciden con los nombres de sus indices. Si la variable existe, la sobreescribe
    if($tipo=="submit"){
        echo "<button type=\"submit\" class=\"btn btn-primary\" name=\"$nombre\">$texto</button>";
    }else{
        echo '<div class="form-group row">';
        echo "<label for=\"$id\" class=\"col-sm-2 col-form-label\">$texto</label>";
        echo '<div class="col-sm-10">';
        echo "<input type=\"$tipo\" class=\"form-control\" id=\"$id\" name=\"$nombre\" placeholder=\"$placeholder\">";
        echo "</div></div>";
    }
    
}




    
    
      
        
        


