<?php
include "componentes/componentes.php"; //no es una funcion. 
////las variable que estan aqui estaran tambien en la vista llamada
?>

<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }

        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <?php

        function accionIndex() {
            include "views/formulario.php";
        }

        function accionComprobar() {
            
            $errores = [];
            $hoy = date("Y"); //Devuelve string con el formato de fecha que se quiera
            $anno_fecha = explode("-", $_REQUEST["fecha"])[0]; //Devuelve un array donde cada elemento 
            //es un string entre los guiones de fecha
            if (strlen($_REQUEST["nombre"]) == 0 || strlen($_REQUEST["nombre"]) > 5) {
                $errores[] = "El nombre esta mal";
            }

            if ($anno_fecha == $hoy || $anno_fecha <= $hoy - 10) {
                //el request tiene formato fecha año-mes-dia cuando lo recoge
                $errores[] = "La fecha no es correcta";
            }
           
            if (count($errores) == 0) {
                var_dump($_REQUEST);
                $resultados =[
                    "titulo"=>"Los datos introducidos son",
                    "datos"=>$_REQUEST
                ];
            } else {
                $resultados =[
                    "titulo"=>"Errores",
                    "datos"=>$errores
                ];
                
            }
            include "views/listado.php"; //Carga la vista para ventana modal
        }

        /* Esto es la logica de control, simulada con el boton de añadir */
        if (isset($_REQUEST["anadir"])) {
            //comprobar accion de usuario.Esta es la que lo tiene que hacer
            accionComprobar();
        } else {
            accionIndex();
        }
        ?>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>


    </body>
</html>

