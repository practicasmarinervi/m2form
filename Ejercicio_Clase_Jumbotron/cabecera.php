<?php
//Definicion de la funcion cabecera, funcion de la vista
function cabecera($arg){
    echo '<div class="page-header">';
    echo "<h1>$arg</h1>";
    echo "</div>";
}


//por standard aqui no es necesario cerrar etiqueta php(la ultima)
//como se carga dentro de php, no da problemas 
//la etiqueta de abrir no es opcional