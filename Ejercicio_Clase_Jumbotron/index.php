<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Ejercicio_Clase</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

        <?php
        include "cabecera.php";
        cabecera("Ejemplo de Formulario"); //Llama a la funcion cabecera

        if (!isset($_REQUEST["enviar"])) {
            include "contenido.php";

            //Array con la cabecera y el texto, arg de la funcion jumbotron
            jumbotron([
                "cabecera" => "Ejemplo",
                "texto" => ["primero", "segundo", "tercero"]
            ]);
            
            //Creo variables con los campos del formulario
            $nombre=[
                "nombre" => "nombre",
                "texto" => "Nombre",
                "id" => "iNombre",
            ];
            $fecha=[
                "nombre" => "fecha_nacimiento",
                "texto" => "Fecha Nacimiento",
                "id" => "iFecha",
            ];
            $boton=[
                "texto" => "Añadir",
                "tipo" => "submit",
                "nombre" => "enviar"
            ];
            //Creo un array con los arrays de los campos
            $campos = array($nombre, $fecha, $boton);
            include "formulario.php";
            controles($campos);
            
            echo "</form></div>";
        } else {
            include "resultados.php";
        }
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>
    </body>
</html>
