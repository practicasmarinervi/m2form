<?php

function crearFormulario($datos) {
    echo '<form class="form-horizontal">';

    foreach ($datos as $key => $value) {//key son nombre y fecha
        control($key, $value);
    }
    echo '</form>';
}

function control($k, $v) {
    $nombre = $k;
    $tipo = "text";
    $placeholder = " ";
    $id = " ";
    $texto = " ";
    extract($v); //pone la cte por defecto
    if ($tipo != "submit") {
        echo '<div class = "form-group">';
        echo "<label for =\"$id\" class = \"col-sm-2 control-label\">$texto</label>";
        echo '<div class = "col-sm-10">';
        echo "<input type =\"$tipo\" class =\"form-control\" id = \"$id\" name=\"$nombre\" placeholder =$placeholder>";
        echo "</div> </div>";
    } else {
        echo '<div class="form-group">';
        echo '<div class="col-sm-offset-2 col-sm-10">';
        echo "<button name=\"$nombre\" type=\"$tipo \" class=\"btn btn-default\"> $texto </button>";
        echo "</div>  </div>";
        //El boton debe tener name obligatoriamente para que este dentro del formulario
    }
}

function crearModal($r) {

    echo '<div id="ventana" class="modal fade" tabindex="-1" role="dialog">';
    echo '<div class="modal-dialog" role="document">';
    echo '<div class="modal-content">';
    echo '<div class="modal-header">';
    echo '<button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span aria-hidden = "true">&times
    </span></button>';
    echo '<h4 class = "modal-title">' . $r["titulo"] . '</h4>';
    echo '</div>';
    echo '<div class = "modal-body">';

    echo "<ul>";
    foreach ($r["datos"]as $k => $v) {
        if ((string) $k != "anadir") //Para que no salga el boton en resultados
            echo "<li> $k:$v </li>";
    }
    echo "</ul>";

    echo '</div>';
    echo '<div class = "modal-footer">';
    echo '<button type ="button" class = "btn btn-default" data-dismiss = "modal">Cerrar</button>';
    echo '<button type = "button" class = "btn btn-primary">Save changes</button>';
    echo '</div>';
    echo '</div>'; //.modal-content -->
    echo '</div>'; //.modal-dialog -->
    echo '</div>'; //.modal -->
}

function crearListaRegistros($reg) {
    echo '<div class="container">';
    echo '<form><ul class="list-group">';
    
    foreach ($reg as $registro=>$valores) {//Es un array bidimensional, dos bucles
        foreach ($valores as $indice => $valor) {
            if ($indice != "anadir") {//Esto para que no me saque "enviar" en los resultados
                echo "<li class=\"list-group-item\">$indice: $valor";
                if($indice=="nombre"){
                    echo '<button type="submit" value='.$registro.' name="borrar" class="close" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>';
                }
            echo "</li>";
            }
            
        }
    }
    echo "</ul></form></div>";
}

function eliminarRegistro($n){
    
    unset($_SESSION["registros"][$n]);
    include "views/registros.php";
}
