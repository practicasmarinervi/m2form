<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <?php
        include "cabecera.php"; //carga tanto codigo php como html, de forma fija
        cabecera("Ejemplo de clase"); //LLamada a la funcion (mejor en el index)
        //Logica de Control de funcionamiento de la app
        if (!isset($_REQUEST["enviar"])) {
            include "contenido.php"; //cargamos este include solo cuando sea necesario
           
            /* Llamada a la funcion Jumbotron
             * Se le pasan 2 argumentos (cabecera y textos)
             * Se le pasan los argumentos como arrays en el orden que queramos, donde el indice del array
             * es el elemento que se le pasa, un string y un array
             */
            jumbotron([
                "cabecera" => "Ejemplo",
                "texto" => ["primero", "segundo", "tercero"]
            ]);

            echo '<div class="container"><form>';//Crea el contenedor para el formulario
            /*Llamada a la funcion control
             * Los elementos que no estan en el array asociativo, toma su valor por defecto
             */
            control([
                "nombre" => "nombre",
                "texto" => "Nombre",
                "id" => "iNombre",
            ]);
            control([
                "nombre" => "apellidos",
                "texto" => "Apellidos",
                "id" => "iApellidos",
            ]);
            control([
                "nombre" => "edad",
                "texto" => "Edad",
                "id" => "iEdad",
                "tipo" => "number"
            ]);
            
            control ([
                "nombre" => "aficion",
                "texto" => "Aficion",
                "id" => "iAficion",
                "placeholder" => "introduce tu aficion"
            ]);

            control([
                "texto" => "Enviar",
                "tipo" => "submit",
                "nombre" => "enviar"
            ]);
            
            

            echo "</form></div>";
        } else {
            include "resultados.php";
        }
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>
    </body>
</html>
